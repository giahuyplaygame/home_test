<?php

require_once 'order.php';
require_once 'define_coefficient.php';

class Item
{
    protected $weight;
    protected $width;
    protected $height;
    protected $depth;

    public function __contruct($weight, $height)
    {
        $this->weight = $weight;
        $this->width = $width;
        $this->height = $height;
        $this->depth = $depth;
    }

    private function itemPrice(){
        $itemPrice = $this->amazonPrice() + $this->shippingFee();
        return  $itemPrice;
    }

    private function shippingFee($type = 0){
        $shippingFee = 0;
        if($type == 0){
            $shippingFee = max($this->feeByWeight(), $this->feeByDimensions());
        }else{
            $shippingFee = max($this->feeByWeight(), $this->feeByDimensions(), $this->feeByProductType());
        }
        return  $shippingFee;
    }

    private function feeByWeight(){
        $feeByWeight = $this->weight * weightCoefficient;
        return  $feeByWeight;
    }

    private function feeByDimensions(){
        $feeByDimensions = $this->width * $this->height * $this->depth *  dimensionCoefficient;
        return  $feeByDimensions;
    }
    
    private function feeByProductType(){
       //
    }
}
